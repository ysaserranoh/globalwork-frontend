import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      toolbar: true
    }
  },
  {
    path: '/candidates',
    name: 'candidates',
    component: () => import('../views/misc/Candidates.vue'),
    meta: {
      footer: true
    }
  },
  {
    path: '/details/:id',
    name: 'details',
    component: () => import('../views/misc/Details.vue'),
    meta: {
      footer: true
    }
  },
  {
    path: '/exercises',
    name: 'exercises',
    component: () => import('../views/misc/exercises.vue'),
    meta: {
      ex: true,
      footer: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
